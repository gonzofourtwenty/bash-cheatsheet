# First install pdflatex, latex and everything else
sudo apt-get install texlive texlive-lang-german texlive-doc-de texlive-latex-extra 

# Install pandoc for printing .md to .pdf
sudo apt-get install pandoc

# Generate pdf from valid .md
pandoc originalfile.md -o newpdf.pdf

# Open pdf via terminal
gnome-open file.pdf

##############################################################
# For a markdown tutorial, open file mdref.txt	             #
# For a layout tutorial for this cheatsheet, open layout.txt #
# For anything else, visit www.w3schools.com                 #
##############################################################
