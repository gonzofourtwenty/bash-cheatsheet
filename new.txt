Get own public ip
curl https://ipinfo.io/ip


Download from youtube in current folder
youtube-dl -x --audio-format mp3 --audio-quality 0 -o "%(title)s.%(ext)s"  "https://www.youtube.com/watch?v=58HQeed8Vhg"

play music in terminal
ffplay xxx.mp3

change screen zoom
gsettings set org.gnome.desktop.interface text-scaling-factor 1.5


copy all files one folder up
cd folder/to/copy
mv * ../



list dir size
du -hs /path/to/file


To copy from your local computer to the remote, type, in the local computer:

scp /tmp/file user@example.com:/home/name/dir

(where /tmp/file can be replaced with any local file and /home/name/dir with any remote directory)

To copy from the remote computer to the local one, type, in the local computer:

scp user@example.com:/home/name/dir/file /tmp
