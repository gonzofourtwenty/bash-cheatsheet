# Bash Cheatsheet
&nbsp;
&nbsp;
&nbsp;
&nbsp;


### Update Software and OS

1. Basic Software and OS update commands
    * ```sh
sudo apt-get update
```
    * ```sh
sudo apt-get upgrade
```
    * ```sh
sudo apt-get dist-upgrade
```
&nbsp;
2. Software installation
    * ```sh
sudo apt-get install packagename
```
&nbsp;
3. Update specific Software
    * ```sh
sudo apt-get install --only-upgrade packagename
```
&nbsp;
4. Extract .tar.gz file
    * ```sh
tar xvf filename.tar.gz
```
&nbsp;
5. Create new tar archive
    * ```sh
tar cvf archive_name.tar dirname/
```
&nbsp;
6. Backup with tar, you can exclude anything you want, just give the path
    * ```
sudo tar czf /backup.tar.gz --exclude=/backup.tar.gz --exclude=/media --exclude=/mnt /
```
&nbsp;
&nbsp;
&nbsp;


### Search and Find

1. This bash script looks for the last updated files, sorts them, and filters for a specific string
    * ```sh
sudo find /home -printf '%T+ %p\n' | sort -r | head -n 100 | grep "string"
```
&nbsp;
2. Simpel search for specific filename
    * ```sh
sudo find | grep "string"
```
    * ```sh
sudo find | grep "string" | head -n 100
```
&nbsp;
3. Search for a given string in a file
    * ```sh
grep -i "string" demofile
```
&nbsp;
4. Print the matched line, along with the 3 lines after it
    * ```sh
grep -A 3 -i "string" demofile
```
&nbsp;
5. Search for a string recursively
    * ```sh
grep -r "string" *
```
&nbsp;
6. List files with depth (/var = 1, /var/test = 2 ... ) to show disk usage of each file or dir
    * ```sh
sudo du -xh --max-depth=3 / | sort -gr | head -n 50
```
&nbsp;
7. Search for a string recursivley and print original files line number
    * ```sh
cat file.txt | grep -nr "test"
```

&nbsp;
&nbsp;
&nbsp;


### Calculator

1.  Mighty Calculator
    * ```sh
echo "27*6" | bc
```
&nbsp;
&nbsp;
&nbsp;


### Bash Workflow Optimization

1. Connect and chain multiple commands on the Bash
    * ```sh 
cp file1 file2 ; cp file1 file3 ; rm file1
```
&nbsp;
2. If you require that the individual commands MUST complete before the next can be started, then you'd use && instead:
    * ```sh
cp file1 file2 && cp file1 file3 && rm file1
```
&nbsp;
3. Check Bash History
    * ```sh
history
```
    * ```sh
history | grep "string"
```
&nbsp;
4. Show specific entry in command history, find command	
    * ```sh
fc -l 1400 1405
```
&nbsp;
&nbsp;
&nbsp;


### System Monitoring

1. Shows mem, cpu usage and processes, mighty command
    * ```sh
top
```
&nbsp;
2. Show CPU Informations
    * ```sh
lscpu
```
&nbsp;
3. Show Hardware Information
    * ```sh
lshw 
```
    * ```sh
hwinfo
```
&nbsp;
4. Check disk space usage
    * ```sh
df
```
&nbsp;
5. Show screen resolution
    * ```sh
xdpyinfo | grep dimensions
```
&nbsp;
&nbsp;
&nbsp;


### File Monitoring and File operations

1. Watch a file live, changes are instantly visible
    * ```sh
tail -f logfile.log
```
&nbsp;
2. Remove a directory and all subdirs even they are not empty
    * ```sh
rm -rf dirpath
```
&nbsp;
3. Remove all files in current working directory    
    * ```sh
rm *
```
&nbsp;
4. Create a new, simple directory
    * ```sh
mkdir newdir
```
&nbsp;
5. List all files in last modified and human readable order
    * ```sh
ls -larth
```
&nbsp;
6. Copy all png files to folder
    * ```sh
find . -name "*.png" -exec cp {} /home/user/folder \;
```
&nbsp;
&nbsp;
&nbsp;


### Git Commands

1. Full Command chain on a simple push/pull project
    * ```sh
git pull repolinkhere
```
    * ```sh
git status
```
    * ```sh
git add path/file
```
    * ```sh
git commit -m "commit message"
```
    * ```sh
git push
```
&nbsp;
2. Show Commit History
    * ```sh
git log
```
&nbsp;
3. Keep changes, stash everything else
    * ```sh
git stash save --keep-index
```
&nbsp;
4. Add everything in stage (not recommended, dangerous)
    * ```sh
git add --all
```
&nbsp;
5. Discard changes from a file in working directory (to avoid conflicts) Be careful!
    * ```sh
git checkout -- path/to/file
```
&nbsp;
6. Show specific commit informations
    * ```sh
git log
```
    * ```sh
git show e7bx6
```
&nbsp;
&nbsp;
&nbsp;


### Text Processing

1. Nano Editor
    * ```sh
nano test.txt
```

>*Ctrl+W	search string, jump to string with enter*

>*Alt+W		jump to next found String (inside Ctrl+W search mode)*

>*Ctrl+O	save file with enter*

>*Ctrl+X	close editor/file with enter*

>*Ctrl+K	cuts current highlighted line*

>*Ctrl+U	paste cutted line again*

&nbsp;

2. VI Editor
    * ```sh
vi file.txt
```

>*Lots of commands use vi for monitoring data, identifiable by a : in the command line*

>*:x	save and quit editor*

>*:q	quit editor without saving changes*

&nbsp;

3. Generate PDF from valid markdown (need to download dependencies first, pandoc and tex-live)
    * ```sh
pandoc originalfile.md -o newfile.pdf
```
&nbsp;
4. Show PDF via Terminal
    * ```sh
gnome-show file.pdf
```
&nbsp;
5. Simple text output
    * ```sh
cat file.txt
```

>*-n	Line numbers on the left side*

&nbsp;

6. Compare two files
    * ```sh
diff file1.txt file2.txt
```
&nbsp;
&nbsp;
&nbsp;


### Dependency Management (Software Development)

1. Maven (run in project folder with maven hiearchy/files) 
    * ```sh
mvn --help
```
    * ```sh
mvn
```
    * ```sh
mvn compile test -Dtest=com.example.TestReporting\#testClassName
```
    * ```sh
sudo mvn clean install
```
    * ```sh
mvn install -Dmaven.test.skip=true
```
&nbsp;
2. NPM (Node Package Manager, run in project folder). Install dependencies and typings, then run
    * ```sh
npm install
```
    * ```sh
npm run typings-install
```
    * ```sh
npm run
```
&nbsp;
&nbsp;
&nbsp;


### User and Session

1. Shutdown Timer in 30 minutes
    * ```sh
shutdown -h 30
```
&nbsp;
2. Create executable symbol with link to "startmenu". Write text in file
    * ```sh
sudo nano /usr/share/applications/newProgramm.desktop
```

>[Desktop Entry]

>Name=ADT

>Type=Application

>Exec=/home/dusan/adt-bundle-linux-x86_64-20140321/eclipse/eclipse 

>Terminal=false

>Icon=/home/dusan/adt-bundle-linux-x86_64-20140321/eclipse/eclipse.xpm

>Comment=Any comment

>NoDisplay=false

>Categories=Development;IDE

>Name[en]=adt.desktop

&nbsp;
&nbsp;
&nbsp;


### Right and Role Management

1. Make a file executable
    * ```sh
chmod +x file
```
&nbsp;
&nbsp;
&nbsp;


### Useful Bash Software

1. Dictionary "dict"
    * ```sh
dict Wort
```
&nbsp;
2. Very mighty Comparison Tool "meld"
    * ```sh
meld folder1 folder2
```
&nbsp;
3. Get lots of different terminal styles
    * ```sh
wget -O gogh https://git.io/vKOB6 && chmod +x gogh && ./gogh
```
&nbsp;
4. "progress" Show the progress of running mv or cp tasks
    * ```sh
sudo apt-get install progress
```
    * ```sh
progress -M
```
&nbsp;
&nbsp;
&nbsp;


### Useful Commands on Linux

1. Open Terminal

>*Ctrl+AltLeft+T*

2. Maximize Terminal (current window)

>*AltLeft+Space then X*

3. Minimize Terminal (current window)

>*AltLeft+Space then N*

4. Close Terminal (current window)

>*AltLeft+Space then C*

5. Stop a running task in terminal

>*Ctrl+C*

6. Switch betweens desktops

>*Ctrl+Alt+ArrowKey*

7. Move active Window to other desktop

>*Ctrl+Alt+Shift+ArrowKey*
&nbsp;
&nbsp;
&nbsp;


### Useful Commands in the Terminal

1. Search through command history

>*Ctrl+R*

&nbsp;
&nbsp;
&nbsp;


### Important Files on Linux

1. Log Files in /var/log
    * ```sh
cd /var/log
```

>*apt/		history.log logs all package installation and removal information*

>*dist-upgrade/	apt.log logs all distribution information during upgrades*

>*installer/	log files created during installation*

>*apport.log	saves information about system crashes*

>*auth.log	information about authentication activities (e.g sudo)*

>*boot.log	booting information*

>*kern.log	kernel informations, warnings and errors*



2. Access Storage Devices in /media/
    * ```sh
cd /media/SeagateBackupHDD
```
